## MS-ITEMS

Esto es una prueba de concepto que demuestra como implementar un microservicio usando Spring Boot, Kafka Y Docker.

Contiene la lógica general de entrada de usuarios, registro, obtencion y actualizacion de items en base de datos, ademas por cada vez que se realize un registro o actualizacion del item se envia un mensaje a la cola kafka, llamandose a este microservicio como productor.

Method	| Path	| Description	| User authenticated (Token JWT)	| 
------------- | ------------------------- | ------------- |:-------------:|
POST	| /api/v1/item/add	| Add Item in BD	| × | 
GET	| /api/v1/item/{id}	| Get item by id	| x  | 	
PUT	| /api/v1/item/{id}	| Update item in BD	| × | 
POST	| /api/v1/authenticate	| Login	|   | 

#### Notas

- Es un API REST que se comunica con otro microservicio de manera asincrona a traves de una cola kafka.
- Se usa PostgreSQL como base primaria para todos los endpoints.

## ¿Como correr este microservicio?

Hay que iniciar con docker estas 4 aplicaciones Spring Boot (MS-ITEMS), PostgreSQL, Kafka y Zookeper.

#### Antes de que inicies

- Instala Docker y Docker Compose.
- DBeaver, como cliente de base datos
- Offset Explorer 2, permite visualizar los objetos y navegar con los objetos en tu Apache Kafka Cluster.

#### Ponlo a correr

1. Clonar el proyecto: `git clone https://dwilsonc@bitbucket.org/dwilsonc/ms-items.git`

2. Posicionarse sobre la ruta donde se encuentra el dockerfile y ejecutar el siguiente comando docker para construir la imagen del microservicio MS-ITEMS

   `docker build -t ms-items .`
   
3. Iniciemos el servidor Kafka y PostgreSQL, escribiendo el comando `docker-compose up -d` desde donde se encuentra ubicado el fichero `docker-compose.yml`.

    
    Opcional

    Podemos usar el comando nc para verificar que ambos servidores (kafka y Zookeper) estén escuchando en los puertos respectivos:

   `nc -z localhost 22181`

    Connection to localhost port 22181 [tcp/*] succeeded!

   `nc -z localhost 29092`

    Connection to localhost port 29092 [tcp/*] succeeded!

#### Ejecucion en local

Ir a la carpeta `/src/postman` e importar la coleccion en postman.

- [http://localhost:8084/nisum](http://localhost:8084/nisum) - Aplicación Spring Boot

#### Usuario Prueba Login

usuario: `dwilsonc`

clave: `123456Ab`

### Swagger

Puedes consultar los endpoints accediendo a swagger: 

- [http://localhost:8084/nisum/swagger-ui.html](http://localhost:8084/nisum/swagger-ui.html)

### Cobertura

Generado desde la carpeta `/build/jacocoHtml/index.html`

![jacoco.png](jacoco.png)

