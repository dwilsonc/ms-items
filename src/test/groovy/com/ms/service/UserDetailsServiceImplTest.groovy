package com.ms.service

import com.ms.entity.UserEntity
import com.ms.repository.UserRepository
import com.ms.service.impl.UserDetailsServiceImpl
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UsernameNotFoundException
import spock.lang.Specification

class UserDetailsServiceImplTest extends Specification {

    private UserDetailsServiceImpl userDetailsServiceImpl = new UserDetailsServiceImpl()
    UserRepository userRepository = Mock()

    void setup() {
        this.userDetailsServiceImpl.userRepository = this.userRepository
    }

    def "Usuario encontrado"() {
        given:
        userRepository.findByUsername("dwilsonc") >> UserEntity.builder().username("dwilsonc").password("1234").build()

        when: "ejecuto la funcionalidad"
        UserDetails resp = userDetailsServiceImpl.loadUserByUsername("dwilsonc")

        then: "resultado esperado"
        resp
    }

    def "Usuario no encontrado"() {
        given:
        userRepository.findByUsername("dwilsonc") >> null

        when: "ejecuto la funcionalidad"
        userDetailsServiceImpl.loadUserByUsername("dwilsonc")

        then: "resultado esperado"
        thrown(UsernameNotFoundException)
    }
}