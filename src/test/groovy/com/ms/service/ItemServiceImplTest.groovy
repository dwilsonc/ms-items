package com.ms.service

import com.ms.dto.ImagesDto
import com.ms.dto.ItemRequestDto
import com.ms.dto.ItemResponseDto
import com.ms.entity.CurrencyEntity
import com.ms.entity.ImageEntity
import com.ms.entity.ItemEntity
import com.ms.exception.NotFoundException
import com.ms.repository.CurrencyRepository
import com.ms.repository.ImagesRepository
import com.ms.repository.ItemRepository
import com.ms.service.impl.ItemServiceImpl
import spock.lang.Specification

class ItemServiceImplTest extends Specification {

    private ItemServiceImpl itemServiceImpl = new ItemServiceImpl()
    ItemRepository itemRepository = Mock()
    CurrencyRepository currencyRepository = Mock()
    ImagesRepository imagesRepository = Mock()
    List<ImagesDto> images = new ArrayList<>()

    void setup() {
        this.itemServiceImpl.itemRepository = this.itemRepository
        this.itemServiceImpl.currencyRepository = this.currencyRepository
        this.itemServiceImpl.imagesRepository = this.imagesRepository

        images.add(ImagesDto.builder().id(1).url("http://cloud.images.google.com/pelotaplayaazul41.png).build() "))
        images.add(ImagesDto.builder().id(1).url("http://cloud.images.google.com/pelotaplayaazul41.png).build()"))
        images.add(ImagesDto.builder().id(1).url("http://cloud.images.google.com/pelotaplayaazul41.png).build()"))
    }

    def "[Add Item] Add Item in BD"() {
        given:
        Optional<CurrencyEntity> result = Optional.of(CurrencyEntity.builder().build());
        currencyRepository.findByCode(_) >> result

        and: "Se mockea itemRepository"
        itemRepository.save(_) >> ItemEntity.builder().build()

        and: "Se mockea imagesRepository"
        imagesRepository.save(_) >> ImageEntity.builder().build()

        when: "ejecuto la funcionalidad"
        ItemResponseDto resp = itemServiceImpl.save(ItemRequestDto.builder().images(images).build())

        then: "resultado esperado"
        resp
    }

    def "[Add Item] Moneda no encontrada"() {
        given:
        currencyRepository.findByCode(_) >> new Optional<CurrencyEntity>()

        when: "ejecuto la funcionalidad"
        itemServiceImpl.save(ItemRequestDto.builder().currency("LIB").build())

        then: "resultado esperado"
        thrown(NotFoundException)
    }

    def "[Get Item] Get Item by ID"() {
        given:
        Optional<ItemEntity> result = Optional.of(ItemEntity.builder().id(1).build());
        itemRepository.findById(1) >> result

        when: "ejecuto la funcionalidad"
        ItemResponseDto resp = itemServiceImpl.findById(1)

        then: "resultado esperado"
        resp
    }

    def "[Get Item] Id no encontrado"() {
        given:
        itemRepository.findById(1) >> new Optional<ItemEntity>()

        when: "ejecuto la funcionalidad"
        itemServiceImpl.findById(1)

        then: "resultado esperado"
        thrown(NotFoundException)
    }

    def "[Update Item] Update Item in BD"() {
        given:
        Optional<ItemEntity> result1 = Optional.of(ItemEntity.builder().id(1).build());
        itemRepository.findById(1) >> result1
        Optional<CurrencyEntity> result2 = Optional.of(CurrencyEntity.builder().build());
        currencyRepository.findByCode(_) >> result2

        and: "Se mockea itemRepository"
        itemRepository.save(_) >> ItemEntity.builder().build()

        and: "Se mockea imagesRepository"
        imagesRepository.save(_) >> ImageEntity.builder().build()

        when: "ejecuto la funcionalidad"
        ItemResponseDto resp = itemServiceImpl.update(ItemRequestDto.builder().images(images).build(),1)

        then: "resultado esperado"
        resp
    }

    def "[Update Item] Moneda no encontrada"() {
        given:
        Optional<ItemEntity> result = Optional.of(ItemEntity.builder().id(1).build());
        itemRepository.findById(1) >> result
        currencyRepository.findByCode(_) >> new Optional<CurrencyEntity>()

        when: "ejecuto la funcionalidad"
        itemServiceImpl.update(ItemRequestDto.builder().currency("LIB").build(),1)

        then: "resultado esperado"
        thrown(NotFoundException)
    }

    def "[Update Item] Item no encontrado"() {
        given:
        itemRepository.findById(_) >> new Optional<ItemEntity>()

        when: "ejecuto la funcionalidad"
        itemServiceImpl.update(ItemRequestDto.builder().build(),1)

        then: "resultado esperado"
        thrown(NotFoundException)
    }
}