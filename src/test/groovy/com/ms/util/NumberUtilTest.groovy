package com.ms.util

import spock.lang.Specification
import spock.lang.Unroll


class NumberUtilTest extends Specification {

    @Unroll
    def "Valida que sea solo numeros"() {
        expect:
        boolean resultado = NumberUtil.validateOnlyNumbers(input);
        output == resultado

        where:
        input | output
        "1" | true
        "23" | true
        "2a" | false
        "3_" | false
    }

    @Unroll
    def "Valida clase"() {
        expect:
        new NumberUtil()
    }
}