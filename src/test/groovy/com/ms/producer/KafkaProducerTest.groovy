package com.ms.producer

import org.springframework.kafka.core.KafkaTemplate
import spock.lang.Specification

class KafkaProducerTest extends Specification {

    KafkaTemplate<String, String> kafkaTemplate = Mock()

    private KafkaProducer kafkaProducer = new KafkaProducer()

    void setup() {
        this.kafkaProducer.kafkaTemplate = this.kafkaTemplate
    }

    def "sending message succeeds"() {
        expect:
        kafkaProducer.sendObject("items.update", KafkaObject.builder().id(1).message("new").build())
    }
}