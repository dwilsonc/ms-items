package com.ms.controller

import com.ms.config.JwtToken
import com.ms.resource.LoginRequestResource
import com.ms.resource.LoginResponseResource
import com.ms.service.impl.UserDetailsServiceImpl
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.core.userdetails.User
import spock.lang.Specification

class LoginControllerTest extends Specification {

    private LoginController loginController = new LoginController()
    AuthenticationManager authenticationManager = Mock()
    JwtToken jwtToken = Mock()
    UserDetailsServiceImpl userDetailsService = Mock()

    void setup() {
        this.loginController.authenticationManager = this.authenticationManager
        this.loginController.jwtToken = this.jwtToken
        this.loginController.userDetailsService = this.userDetailsService
    }

    def "Login with username and password"() {
        given:
        authenticationManager.authenticate(_) >> null
        jwtToken.generateToken(_) >> "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJkd2lsc29uYyIsImV4cCI6MTYyOTEyMjMxMSwiaWF0IjoxNjI5MTIyMDExfQ.sCHYF-dsunFvzUOsmerL-EMfxMY91tPn9-AFixDJFdRzSRbumcMF5MhnAtjl1UR9onCHPmAYDdYu0iwBZkd_Ig"

        and: "se mockea userDetailsService"
        userDetailsService.loadUserByUsername("dwilsonc") >> new User("dwilsonc", "1234",
                new ArrayList<>());

        when: "ejecuto la funcionalidad"
        ResponseEntity<LoginResponseResource> resp = loginController
                .createAuthenticationToken(LoginRequestResource.builder().username("dwilsonc").password("1234").build())

        then: "resultado esperado"
        resp.statusCode.value() == 200
    }
}