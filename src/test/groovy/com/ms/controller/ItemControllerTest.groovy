package com.ms.controller

import com.ms.dto.ItemResponseDto
import com.ms.exception.BadRequestException
import com.ms.producer.KafkaProducer
import com.ms.resource.ItemRequestResource
import com.ms.resource.ItemResponseResource
import com.ms.resource.MessageResponseResource
import com.ms.service.ItemService
import org.springframework.http.ResponseEntity
import spock.lang.Specification

class ItemControllerTest extends Specification {

    private ItemsController itemsController = new ItemsController()
    ItemService itemService = Mock()
    KafkaProducer kafkaProducer = Mock()

    void setup() {
        this.itemsController.itemService = this.itemService
        this.itemsController.kafkaProducer = this.kafkaProducer
    }

    def "[Add Item] Add Item in BD"() {
        given:
        kafkaProducer.sendMessage(_) >> null
        itemService.save(_) >> ItemResponseDto.builder().id(2).build()

        when: "ejecuto la funcionalidad"
        ResponseEntity<MessageResponseResource> resp = itemsController.addItem(ItemRequestResource.builder().name("pelotaplaya").build())

        then: "resultado esperado"
        resp.statusCode.value() == 200
    }

    def "[Add Item] Body is null"() {
        when: "ejecuto la funcionalidad"
        itemsController.addItem(null)

        then: "resultado esperado"
        thrown(BadRequestException)
    }

    def "[Get Item] Get Item By ID"() {
        given:
        itemService.findById(1) >> ItemResponseDto.builder().build()

        when: "ejecuto la funcionalidad"
        ResponseEntity<ItemResponseResource> resp = itemsController.getItem("1")

        then: "resultado esperado"
        resp.statusCode.value() == 200
    }

    def "[Get Item] Invalid Format in PathVariable"() {
        when: "ejecuto la funcionalidad"
        itemsController.getItem("1a")

        then: "resultado esperado"
        thrown(BadRequestException)
    }

    def "[Update Item] Update Item in BD"() {
        given:
        itemService.update(_,2) >> ItemResponseDto.builder().id(2).build()

        when: "ejecuto la funcionalidad"
        ResponseEntity<MessageResponseResource> resp = itemsController.updateItem(ItemRequestResource.builder().name("pelotaplayaverde").build(),2)

        then: "resultado esperado"
        resp.statusCode.value() == 200
    }

    def "[Update Item] Body is null"() {
        when: "ejecuto la funcionalidad"
        itemsController.updateItem(null,1)

        then: "resultado esperado"
        thrown(BadRequestException)
    }
}