DROP TABLE IF EXISTS public.user_entity;
DROP TABLE IF EXISTS public.image_entity;
DROP TABLE IF EXISTS public.item_entity;
DROP TABLE IF EXISTS public.currency_entity;

CREATE TABLE public.user_entity (
	user_id serial NOT NULL,
	"password" varchar(255) NULL,
	username varchar(255) NULL,
	CONSTRAINT user_entity_pkey PRIMARY KEY (user_id)
);

CREATE TABLE public.currency_entity (
	id_currency serial NOT NULL,
	code varchar(255) NULL,
	symbol varchar(255) NULL,
	CONSTRAINT currency_entity_pkey PRIMARY KEY (id_currency)
);

CREATE TABLE public.item_entity (
	id_item serial NOT NULL,
	description varchar(255) NULL,
	"name" varchar(255) NULL,
	price float8 NULL,
	sku varchar(255) NULL,
	thumbnail varchar(255) NULL,
	id_currency int4 NOT NULL,
	CONSTRAINT item_entity_pkey PRIMARY KEY (id_item)
);

ALTER TABLE public.item_entity ADD CONSTRAINT fkglbmpnhhh36ivyhy6jefjwrsl FOREIGN KEY (id_currency) REFERENCES public.currency_entity(id_currency);

CREATE TABLE public.image_entity (
	id_image serial NOT NULL,
	url varchar(255) NULL,
	id_item int4 NOT NULL,
	CONSTRAINT image_entity_pkey PRIMARY KEY (id_image)
);

ALTER TABLE public.image_entity ADD CONSTRAINT fkk8m8jdntnckbcxqxhxdfw0k5 FOREIGN KEY (id_item) REFERENCES public.item_entity(id_item);

INSERT INTO public.user_entity
("password", username)
VALUES('$2a$10$KYGaIzIwSkNDgOoF4nAWYedvcSFBNZ6hIc.dGOpurOA5zKjZ/oRty', 'dwilsonc');
INSERT INTO public.currency_entity
(code, symbol)
VALUES('USD', '$');
INSERT INTO public.currency_entity
(code, symbol)
VALUES('EUR', '€');