package com.ms.exception.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Class ErrorDetails
 *
 * @author denniswilson
 *
 */
@Getter
@Setter
@AllArgsConstructor
public class ErrorDetails {

    /**
     * MESSAGE
     */
    private String message;

}

