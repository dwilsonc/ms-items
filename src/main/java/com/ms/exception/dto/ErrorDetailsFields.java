package com.ms.exception.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Class ErrorDetailsFields
 *
 * @author denniswilson
 *
 */
@Getter
@Setter
public class ErrorDetailsFields {

    /**
     * MESSAGE
     */
    @JsonProperty(value = "message")
    private final String message;

    /**
     * FIELDS
     */
    @JsonProperty(value = "fields")
    private List<InvalidFields> fields = new ArrayList<>();

    public ErrorDetailsFields(String message) {
        this.message = message;
    }

    public void agregarCampoError(String mensaje, String nombreCampo) {
        InvalidFields error = new InvalidFields(mensaje, nombreCampo);
        fields.add(error);
    }

}
