package com.ms.exception.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Class InvalidFields
 *
 * @author denniswilson
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class InvalidFields {

    /**
     * MESSAGE
     */
    @JsonProperty(value = "message")
    private String message;

    /**
     * FIELDNAME
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "field-name")
    private String fieldName;
}
