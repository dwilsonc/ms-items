package com.ms.exception;

/**
 * Class NotFoundException
 *
 * @author denniswilson
 *
 */
public class NotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    private final String msg;

    public NotFoundException(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

}
