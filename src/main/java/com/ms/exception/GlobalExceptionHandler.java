package com.ms.exception;

import com.ms.constantes.Constantes;
import com.ms.exception.dto.ErrorDetails;
import com.ms.exception.dto.ErrorDetailsFields;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.List;

/**
 * Class GlobalExceptionHandler
 *
 * @author denniswilson
 *
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorDetailsFields> methodArgumentNotValidException(MethodArgumentNotValidException ex) {
        BindingResult result = ex.getBindingResult();
        List<org.springframework.validation.FieldError> fieldErrors = result.getFieldErrors();
        ErrorDetailsFields errorDetails = new ErrorDetailsFields(Constantes.INVALID_PARAMETERS);
        for (org.springframework.validation.FieldError fieldError : fieldErrors) {
            errorDetails.agregarCampoError(fieldError.getDefaultMessage(), fieldError.getField());
        }
        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<ErrorDetails> badRequestException(Exception ex) {
        ErrorDetails errorDetails = new ErrorDetails(ex.getMessage());
        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = ValidationException.class)
    public ResponseEntity<ErrorDetails> handleException(ValidationException ex) {
        ErrorDetails errorDetails = new ErrorDetails(ex.getMsg());
        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = NotFoundException.class)
    public ResponseEntity<ErrorDetails> notFoundException(NotFoundException ex) {
        ErrorDetails errorDetails = new ErrorDetails(ex.getMsg());
        return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
    }
}