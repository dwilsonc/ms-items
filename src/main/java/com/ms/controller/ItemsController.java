package com.ms.controller;

import com.ms.dto.ItemResponseDto;
import com.ms.exception.BadRequestException;
import com.ms.exception.NotFoundException;
import com.ms.producer.KafkaObject;
import com.ms.producer.KafkaProducer;
import com.ms.resource.ItemRequestResource;
import com.ms.resource.ItemResponseResource;
import com.ms.resource.MessageResponseResource;
import com.ms.resource.mapper.ItemResourceMapper;
import com.ms.service.ItemService;
import com.ms.util.NumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Class ItemsController
 *
 * @author denniswilson
 *
 */
@RestController
public class ItemsController {

    @Autowired
    private ItemService itemService;

    @Autowired
    private KafkaProducer kafkaProducer;

    @Value("${spring.kafka.topic.request-topic}")
    private String topic;

    /**
     * Method that add item in BD and send response to topic "items-update"
     *
     * @param itemRequestResource
     * @return
     * @throws BadRequestException
     * @throws NotFoundException
     */
    @ResponseStatus(code = HttpStatus.CREATED)
    @PostMapping(value = "/api/v1/item/add")
    public ResponseEntity<MessageResponseResource> addItem(@Valid @RequestBody(required = false) ItemRequestResource itemRequestResource)
            throws BadRequestException, NotFoundException {

        if (itemRequestResource == null)
            throw new BadRequestException("Body Required");

        ItemResponseDto response = itemService.save(ItemResourceMapper.toDto(itemRequestResource));

        kafkaProducer.sendObject(topic, KafkaObject.builder().id(response.getId()).message("new").build());

        return ResponseEntity.ok(MessageResponseResource.builder().id(response.getId()).message("success").build());
    }

    /**
     * Method that get item by id
     *
     * @param id
     * @return
     * @throws BadRequestException
     * @throws NotFoundException
     */
    @GetMapping(value = "/api/v1/item/{id}")
    public ResponseEntity<ItemResponseResource> getItem(@PathVariable("id") String id)
            throws BadRequestException, NotFoundException {

        if(!NumberUtil.validateOnlyNumbers(id))
            throw new BadRequestException("Formato incorrecto, ingrese solo numeros");

        return ResponseEntity.ok(ItemResourceMapper.toResource(itemService.findById(Integer.parseInt(id))));
    }

    /**
     * Method that update item in BD and send response to topic "items-update"
     *
     * @param itemRequestResource
     * @param id
     * @return
     * @throws BadRequestException
     * @throws NotFoundException
     */
    @PutMapping(value = "/api/v1/item/{id}")
    public ResponseEntity<MessageResponseResource> updateItem(@Valid @RequestBody(required = false) ItemRequestResource itemRequestResource,
                                           @PathVariable("id") Integer id)
            throws BadRequestException, NotFoundException {

        if (itemRequestResource == null)
            throw new BadRequestException("Body Required");

        ItemResponseDto response = itemService.update(ItemResourceMapper.toDto(itemRequestResource),id);

        kafkaProducer.sendObject(topic, KafkaObject.builder().id(response.getId()).message("update").build());

        return ResponseEntity.ok(MessageResponseResource.builder().id(response.getId()).message("success").build());
    }
}
