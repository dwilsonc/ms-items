package com.ms.controller;

import com.ms.config.JwtToken;
import com.ms.exception.ValidationException;
import com.ms.resource.LoginRequestResource;
import com.ms.resource.LoginResponseResource;
import com.ms.service.impl.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Class LoginController
 *
 * @author denniswilson
 *
 */
@RestController
public class LoginController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtToken jwtToken;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    /**
     * Method that allows users to enter
     * @param authenticationRequest
     * @return
     * @throws Exception
     */

    @PostMapping("/api/v1/authenticate")
    public ResponseEntity<LoginResponseResource> createAuthenticationToken(@RequestBody LoginRequestResource authenticationRequest)
            throws ValidationException {

        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());

        final String token = jwtToken.generateToken(userDetails);

        return ResponseEntity.ok(new LoginResponseResource(token));

    }

    /**
     * Method authenticate with spring security
     *
     * @param username
     * @param password
     * @throws Exception
     */
    private void authenticate(String username, String password) throws ValidationException {

        try {

            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));

        } catch (BadCredentialsException e) {

            throw new ValidationException("Invalid Credentials!");

        }

    }
}