package com.ms.service;

import com.ms.dto.ItemRequestDto;
import com.ms.dto.ItemResponseDto;
import com.ms.exception.NotFoundException;

/**
 * Interface ItemService
 *
 * @author denniswilson
 *
 */
public interface ItemService {

    public ItemResponseDto save(ItemRequestDto itemRequestDto) throws NotFoundException;

    public ItemResponseDto update(ItemRequestDto itemRequestDto, Integer id) throws NotFoundException;

    public ItemResponseDto findById(Integer id) throws NotFoundException;
}
