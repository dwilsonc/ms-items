package com.ms.service.impl;

import com.ms.dto.ItemRequestDto;
import com.ms.dto.ItemResponseDto;
import com.ms.dto.mapper.ItemDtoMapper;
import com.ms.entity.CurrencyEntity;
import com.ms.entity.ItemEntity;
import com.ms.exception.NotFoundException;
import com.ms.repository.CurrencyRepository;
import com.ms.repository.ImagesRepository;
import com.ms.repository.ItemRepository;
import com.ms.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Class ItemServiceImpl
 *
 * @author denniswilson
 *
 */
@Service
public class ItemServiceImpl implements ItemService {

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private CurrencyRepository currencyRepository;

    @Autowired
    private ImagesRepository imagesRepository;

    /**
     * Method that add item in BD
     *
     * @param itemRequestDto
     * @return
     * @throws NotFoundException
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ItemResponseDto save(ItemRequestDto itemRequestDto) throws NotFoundException {

        CurrencyEntity currencyEntity = currencyRepository.findByCode(itemRequestDto.getCurrency()).orElseThrow(() -> new NotFoundException("No se encontro la moneda - " + itemRequestDto.getCurrency()));

        ItemEntity itemEntity = ItemDtoMapper.toEntity(itemRequestDto);
        itemEntity.setCurrency(currencyEntity);

        ItemEntity itemEntityAfter = itemRepository.save(itemEntity);

        if(itemEntity.getImages()!=null){
            itemEntity.getImages().stream().forEach(image -> {
                image.setItem(itemEntityAfter);
                imagesRepository.save(image);
            });
        }

        return ItemDtoMapper.toDto(itemEntityAfter);
    }

    /**
     * Method that get item by id
     *
     * @param id
     * @return
     * @throws NotFoundException
     */
    @Override
    public ItemResponseDto findById(Integer id) throws NotFoundException {
        ItemEntity itemEntity = itemRepository.findById(id).orElseThrow(() -> new NotFoundException("No se encontro el item - " + id));
        return ItemDtoMapper.toDto(itemEntity);
    }

    /**
     * Method that update item in BD
     *
     * @param itemRequestDto
     * @param id
     * @return
     * @throws NotFoundException
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ItemResponseDto update(ItemRequestDto itemRequestDto, Integer id) throws NotFoundException {

        ItemEntity itemEntityFound = itemRepository.findById(id).orElseThrow(() -> new NotFoundException("No se encontro el Item - " + id));

        CurrencyEntity currencyEntity = currencyRepository.findByCode(itemRequestDto.getCurrency()).orElseThrow(() -> new NotFoundException("No se encontro la moneda - " + itemRequestDto.getCurrency()));

        ItemEntity itemEntity = ItemDtoMapper.toEntity(itemRequestDto);

        itemEntityFound.setCurrency(currencyEntity);
        itemEntityFound.setDescription(itemEntity.getDescription());
        itemEntityFound.setName(itemEntity.getName());
        itemEntityFound.setPrice(itemEntity.getPrice());
        itemEntityFound.setSku(itemEntity.getSku());
        itemEntityFound.setThumbnail(itemEntity.getThumbnail());

        if(itemEntity.getImages()!=null){
            imagesRepository.deleteImagesByItem(itemEntityFound.getId());

            itemEntity.getImages().stream().forEach(image -> {
                image.setItem(itemEntityFound);
                imagesRepository.save(image);
            });
        }

        return ItemDtoMapper.toDto(itemRepository.save(itemEntityFound));
    }
}
