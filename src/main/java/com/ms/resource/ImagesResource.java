package com.ms.resource;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Class ImagesResource
 *
 * @author denniswilson
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ImagesResource {

    /**
     * ID
     */
    private Integer id;

    /**
     * URL
     */
    private String url;
}
