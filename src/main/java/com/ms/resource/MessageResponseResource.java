package com.ms.resource;

import lombok.Builder;
import lombok.Data;

/**
 * Class MessageResponseResource
 *
 * @author denniswilson
 *
 */
@Data
@Builder
public class MessageResponseResource {

    /**
     * ID
     */
    private Integer id;

    /**
     * MESSAGE
     */
    private String message;
}
