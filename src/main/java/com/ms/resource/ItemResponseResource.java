package com.ms.resource;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Class ItemResponseResource
 *
 * @author denniswilson
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ItemResponseResource {

    /**
     * ID
     */
    private Integer id;

    /**
     * SKU
     */
    private String sku;

    /**
     * NAME
     */
    private String name;

    /**
     * PRICE
     */
    private Double price;

    /**
     * CURRENCY
     */
    private CurrencyResource currency;

    /**
     * THUMBNAIL
     */
    private String thumbnail;

    /**
     * DESCRIPTION
     */
    private String description;

    /**
     * IMAGES
     */
    private List<ImagesResource> images;
}
