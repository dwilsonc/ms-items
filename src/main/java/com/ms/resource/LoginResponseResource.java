package com.ms.resource;

import lombok.Data;

import java.io.Serializable;

/**
 * Class LoginResponseResource
 *
 * @author denniswilson
 *
 */
@Data
public class LoginResponseResource implements Serializable {

    private static final long serialVersionUID = -8091879091924046844L;

    /**
     * JWTTOKEN
     */
    private final String jwttoken;

}

