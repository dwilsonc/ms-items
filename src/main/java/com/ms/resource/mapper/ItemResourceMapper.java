package com.ms.resource.mapper;

import com.ms.dto.ItemRequestDto;
import com.ms.dto.ItemResponseDto;
import com.ms.resource.ItemRequestResource;
import com.ms.resource.ItemResponseResource;
import org.modelmapper.ModelMapper;

/**
 * Class ItemResourceMapper
 *
 * @author denniswilson
 *
 */
public class ItemResourceMapper {

    /**
     * Mapper that transform ItemRequestResource to ItemRequestDto
     *
     * @param itemRequestResource
     * @return ItemRequestDto
     */
    public static ItemRequestDto toDto(ItemRequestResource itemRequestResource) {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(itemRequestResource, ItemRequestDto.class);
    }

    /**
     * Mapper that transform ItemResponseDto to ItemResponseResource
     * @param itemResponseDto
     * @return ItemResponseResource
     */
    public static ItemResponseResource toResource(ItemResponseDto itemResponseDto) {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(itemResponseDto, ItemResponseResource.class);
    }
}
