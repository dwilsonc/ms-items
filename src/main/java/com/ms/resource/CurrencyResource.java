package com.ms.resource;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Class CurrencyResource
 *
 * @author denniswilson
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CurrencyResource {

    /**
     * IDE
     */
    private Integer id;

    /**
     * CODE
     */
    private String code;

    /**
     * SYMBOL
     */
    private String symbol;
}
