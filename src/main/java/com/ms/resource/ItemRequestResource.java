package com.ms.resource;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Class ItemRequestResource
 *
 * @author denniswilson
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ItemRequestResource {

    /**
     * SKU
     */
    @NotNull
    @NotBlank
    private String sku;

    /**
     * NAME
     */
    @NotNull
    @NotBlank
    private String name;

    /**
     * PRICE
     */
    @NotNull
    private Double price;

    @NotNull
    private String currency;

    /**
     * THUMBNAIL
     */
    @NotNull
    @NotBlank
    private String thumbnail;

    /**
     * DESCRIPTION
     */
    private String description;

    /**
     * IMAGES
     */
    private List<ImagesRequestResource> images;
}
