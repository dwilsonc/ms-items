package com.ms.resource;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Class LoginRequestResource
 *
 * @author denniswilson
 *
 */
@Data
@Builder
public class LoginRequestResource implements Serializable {

    private static final long serialVersionUID = 5926468583005150707L;

    /**
     * USERNAME
     */
    @NotNull
    @NotBlank
    private String username;

    /**
     * PASSWORD
     */
    @NotNull
    @NotBlank
    private String password;

}
