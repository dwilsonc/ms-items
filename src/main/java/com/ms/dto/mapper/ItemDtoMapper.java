package com.ms.dto.mapper;

import com.ms.dto.ItemRequestDto;
import com.ms.dto.ItemResponseDto;
import com.ms.entity.ItemEntity;
import org.modelmapper.ModelMapper;

/**
 * Class ItemDtoMapper
 *
 * @author denniswilson
 *
 */
public class ItemDtoMapper {

    /**
     * Mapper that transform ItemRequestDto to ItemEntity
     *
     * @param itemRequestDto
     * @return ItemEntity
     */
    public static ItemEntity toEntity(ItemRequestDto itemRequestDto) {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(itemRequestDto, ItemEntity.class);
    }

    /**
     * Mapper that transform ItemEntity to ItemResponseDto
     *
     * @param itemEntity
     * @return ItemResponseDto
     */
    public static ItemResponseDto toDto(ItemEntity itemEntity) {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(itemEntity, ItemResponseDto.class);
    }
}
