package com.ms.dto;

import lombok.*;

/**
 * Class ImagesDto
 *
 * @author denniswilson
 *
 */
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ImagesDto {

    /**
     * ID
     */
    private Integer id;

    /**
     * URL
     */
    private String url;
}
