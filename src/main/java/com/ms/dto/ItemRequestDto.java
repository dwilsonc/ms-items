package com.ms.dto;

import lombok.*;

import java.util.List;

/**
 * Class ItemRequestDto
 *
 * @author denniswilson
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ItemRequestDto {

    /**
     * SKU
     */
    private String sku;

    /**
     * NAME
     */
    private String name;

    /**
     * PRICE
     */
    private Double price;

    /**
     * CURRENCY
     */
    private String currency;

    /**
     * THUMBNAIL
     */
    private String thumbnail;

    /**
     * DESCRIPTION
     */
    private String description;

    /**
     * IMAGES
     */
    private List<ImagesDto> images;
}
