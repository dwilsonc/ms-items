package com.ms.repository;

import com.ms.entity.CurrencyEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Currency Repository
 *
 * @author denniswilson
 */
@Repository
public interface CurrencyRepository extends CrudRepository<CurrencyEntity, Integer> {

    /**
     * Method that find currency by code
     *
     * @param code
     * @return Optional<CurrencyEntity>
     */
    Optional<CurrencyEntity> findByCode(String code);
}
