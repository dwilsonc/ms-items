package com.ms.repository;

import com.ms.entity.ImageEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Images Repository
 *
 * @author denniswilson
 */
@Repository
public interface ImagesRepository extends CrudRepository<ImageEntity, Integer> {

    /**
     * Method that delete images by itemId
     * @param itemId
     */
    @Modifying
    @Query("delete from ImageEntity i where i.item.id = ?1")
    void deleteImagesByItem(Integer itemId);
}
