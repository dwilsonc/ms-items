package com.ms.repository;

import com.ms.entity.ItemEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Item Repository
 *
 * @author denniswilson
 */
@Repository
public interface ItemRepository extends CrudRepository<ItemEntity, Integer> {

}
