package com.ms.util;

import lombok.NoArgsConstructor;

/**
 * Class NumberUtil
 *
 * @author denniswilson
 *
 */
@NoArgsConstructor
public class NumberUtil {

    /**
     * Method that validate only numbers
     * @param cadena
     * @return
     */
    public static boolean validateOnlyNumbers(String cadena){
        return cadena.matches("[+-]?\\d*(\\.\\d+)?");
    }
}
