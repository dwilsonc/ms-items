package com.ms.constantes;

/**
 * Class Constantes
 *
 * @author denniswilson
 *
 */
public class Constantes {

    /**
     * Constant Invalid Parameters
     */
    public static final String INVALID_PARAMETERS = "Parametros invalidos";
}
