package com.ms.entity;

import lombok.*;

import javax.persistence.*;

/**
 * Class ImageEntity
 *
 * @author denniswilson
 *
 */
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class ImageEntity {

    /**
     * ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_IMAGE")
    private Integer id;

    /**
     * ITEM
     */
    @ManyToOne(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_ITEM", referencedColumnName = "ID_ITEM", nullable = false)
    private ItemEntity item;

    /**
     * URL
     */
    @Column(name = "URL")
    private String url;
}
