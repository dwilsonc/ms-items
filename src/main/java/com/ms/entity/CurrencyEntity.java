package com.ms.entity;

import lombok.*;

import javax.persistence.*;

/**
 * Class CurrencyEntity
 *
 * @author denniswilson
 *
 */
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class CurrencyEntity {

    /**
     * ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_CURRENCY")
    private Integer id;

    /**
     * CODE
     */
    @Column(name = "CODE")
    private String code;

    /**
     * SYMBOL
     */
    @Column(name = "SYMBOL")
    private String symbol;
}
