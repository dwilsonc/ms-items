package com.ms.entity;

import lombok.*;

import javax.persistence.*;

/**
 * Class UserEntity
 *
 * @author denniswilson
 *
 */
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class UserEntity {

    /**
     * ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "USER_ID")
    private Integer id;

    /**
     * USERNAME
     */
    @Column(name = "USERNAME")
    private String username;

    /**
     * PASSWORD
     */
    @Column(name = "PASSWORD")
    private String password;

}

