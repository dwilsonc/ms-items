package com.ms.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

/**
 * Class ItemEntity
 *
 * @author denniswilson
 *
 */
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class ItemEntity {

    /**
     * ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_ITEM")
    private Integer id;

    /**
     * SKU
     */
    @Column(name = "SKU")
    private String sku;

    /**
     * CURRENCY
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_CURRENCY", referencedColumnName = "ID_CURRENCY", nullable = false)
    private CurrencyEntity currency;

    /**
     * NAME
     */
    @Column(name = "NAME")
    private String name;

    /**
     * PRICE
     */
    @Column(name = "PRICE")
    private Double price;

    /**
     * DESCRIPTION
     */
    @Column(name = "DESCRIPTION")
    private String description;

    /**
     * THUMBNAIL
     */
    @Column(name = "THUMBNAIL")
    private String thumbnail;

    /**
     * IMAGES
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "item")
    private List<ImageEntity> images;
}
