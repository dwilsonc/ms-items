package com.ms.items;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@SpringBootApplication
@ComponentScan(basePackages = "com.ms.*")
@EnableJpaRepositories("com.ms.*")
@EntityScan(basePackages = "com.ms.*")
@OpenAPIDefinition(info = @Info(title = "MS-ITEMS", version = "2.0", description = "Microservicio items"))
public class ItemsMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ItemsMsApplication.class, args);
	}

}
