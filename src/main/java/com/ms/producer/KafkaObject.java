package com.ms.producer;

import lombok.*;

/**
 * Class KafkaObject
 *
 * @author denniswilson
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class KafkaObject {

    /**
     * ID
     */
    private int id;

    /**
     * MESSAGE
     */
    private String message;
}
