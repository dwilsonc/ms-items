package com.ms.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

/**
 * Class KafkaProducer
 *
 * @author denniswilson
 *
 */
@Component
public class KafkaProducer {

    @Autowired
    private KafkaTemplate<String, KafkaObject> kafkaTemplate;

    /**
     * Method that send object to topic kafka
     * @param topicName
     * @param kafkaObject
     */
    public void sendObject(String topicName, KafkaObject kafkaObject) {
        kafkaTemplate.send(topicName, kafkaObject);
    }
}
